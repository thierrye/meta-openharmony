# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

# This file is intentionally left empty.
# It is needed so the 'require' statement in oniro-openharmony-thirdparty-integration.bb doesn't fail
